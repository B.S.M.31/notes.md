Щоб створити проект, який керуватиметься за допомогою Git, необхідно виконати такі кроки:

Знаходимо на гітлабі кнопку New Project, натискаємо її.

Вигадуємо і вводимо названу назву для проекту, вибираємо доступ - Public, щоб проект за умовчанням був доступний будь-якому користувачеві і нам не доводилося потім комусь спеціально давати до нього доступ. Вибираємо галочку для Readme.

Натискаємо Create project.

Ми створили на гітлабі проект, і тепер сюди відправлятимемо весь код, який написали у себе на комп'ютері.

Там де SSH - вибираємо Https, копіюємо посилання поряд.

Для того, щоб додати нове домашнє завдання в новий репозиторій є 2 шляхи. Перший спосіб: Створення нового Git проекту
У вас немає ні проекту на гітлабі, ні папки на комп'ютері, ви тільки стартуєте роботу, і вирішили розпочати спочатку з підключення гіта до проекту. Для цього після виконання попередніх чотирьох кроків:

На комп'ютері вибираємо місце, де зберігатиметься проект. Наприклад у папці Homeworks. Заходимо в цю папку, натискаємо правою клавішею і вибираємо Git bash here (Windows) або відкриваємо будь-який інший термінал у цьому місці. У цьому місці – значить у папці, в якій вестиметься робота. Якщо при натисканні правої клавіші миші немає можливості відкрити термінал, відкрийте його будь-яким іншим способом (пошукайте в інтернеті інструкцію для вашої операційної системи).
У консолі пишемо команду – git clone  та вставляємо посилання, яке ви скопіюваи там де SSH - вибираємо Https.
Якщо система запитує ім'я користувача та пароль – вводимо своє ім'я користувача – те, яке ви створювали при реєстрації на гітлабі, вводимо свій пароль – той, який створювали при реєстрації на гітлабі.
Якщо все було виконано правильно, всередині робочої папки має бути створена ще одна - з назвою проекту і файлом readme.md всередині. Пропускаємо опис Варіант 2 та переходимо до Кроку 2.
Для внесення наступних змін до проекту переходимо до наступного однойменного розділу.

Варіант 2. Підключення існуючого проекту до Git
У вас є папка з файлами на комп'ютері, але немає проекту на гіті, і ви хочете відправити свою роботу на гітлаб. Для цього після виконання попередніх чотирьох кроків:

Відкриваємо редактор коду,в редакторі коду відкриваємо папку де зберігається наш проект.Відкриваємо термінал у цій папці.
Пишемо команду git init (Ми сказали гіту - створи в цій папці локальний репозиторій і працюй тут, слідкуй за змінами в коді проекту і т.д.).

Пишемо команду git remote add origin та вставляємо посилання яке там де SSH - вибираємо Https
Ми сказали гіту - ця папка (репозиторій) буде синхронізована з папкою на гітлабі, та всі зміни в коді ми будемо відправляти саме туди (для цього ми і написали її адресу - той, що був у засланні).

Тепер переходимо до наступного кроку - "Внесення наступних змін до проекту". Це потрібно, щоб відправити всі файли, які ви вже маєте в проекті, на гітлаб.

Внесення наступних змін до проекту
Дані кроки необхідно виконувати щоразу після того, як ви зробили якісь зміни у проекті, щоб надіслати ці зміни на віддалений репозиторій.

Перебуваючи в папці з проектом, пишемо команду git add . (git add, пропуск, точка). Ми тільки-но сказали гіту - додай усі змінені файли до комміту. Усі файли, в яких ми робили будь-які зміни, будуть спеціальним чином відзначені гітом і підготовлені до комміту.
Пишемо команду git commit -m "Initial commit" (фраза в скобах називається commit message - коментар, який потрібен для короткого опису змін, які ви зробили. Він обов'язковий, і він має бути англійською). Git робить комміт і зберігає всі раніше зроблені зміни у вашому локальному репозиторії.
Далі - git push -u origin master. Ця команда надсилає наш коміт на віддалений репозиторій.
Якщо система запитує ім'я користувача та пароль – вводимо своє ім'я користувача – те, яке створювали при реєстрації на гітлабі, вводимо свій пароль – той, який створювали при реєстрації на гітлабі.
Оновлюємо проект на гітлабі (оновити сторінку браузера) – бачимо, що остання версія файлів, які знаходяться на комп'ютері в нашій папці – тепер є на гітлабі.